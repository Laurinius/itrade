import os
import configparser
import shlex
import re
import glob
import sys

VERSION = '1.1.0'
TRADE_COMMANDS = ['-h', '--help', 'buildcache', 'buy', 'export', 'import', 'local', 'market', 'nav', 'olddata', 'rares',
                        'run', 'sell', 'shipvendor', 'station', 'trade', 'update']
PREDEFINED_VARIABLES = ['sys']

config = configparser.ConfigParser()


def get_system(log_path):
    if not os.path.isdir(log_path):
        return ''
    match_system = '^{[\S]*}\sSystem:'
    find_name = 'System:["].+?["]'
    newest_log = max(glob.iglob(log_path + '/netLog*.log'), key=os.path.getctime)
    for line in reversed(open(newest_log).readlines()):
        if re.match(match_system, line):
            match = re.search(find_name, line)
            if match:
                return match.group(0).strip('System:"').strip('"')
    return ''


def call_trade(parameter):
    command_parameter_string = ''
    system = None
    for par in parameter:
        if ';sys;' in par:
            if system is None:
                if 'logpath' not in config['MAIN']:
                    system = ''
                else:
                    log_path = config['MAIN']['logpath']
                    system = get_system(log_path)
                if len(system) == 0:
                    system = input('System not found. Enter system name.\n>>? ')
                else:
                    o_sys = input('Use [%s] as system? [Press Enter or type other system name]\n>>? ' % (system,))\
                        .strip()
                    if len(o_sys) > 0:
                        system = o_sys
            par = par.replace(';sys;', system)
        for key in config['VARIABLES']:
            if ';' + key + ';' in par:
                par = par.replace(';' + key + ';', config['VARIABLES'][key])
        if ' ' in par:
            command_parameter_string += '"' + par + '"'
        else:
            command_parameter_string += par
        command_parameter_string += ' '
    command_parameter_string = command_parameter_string.strip()
    print('>>! %s' % (command_parameter_string,))
    os.system('""' + config['MAIN']['tradepath'] + '/trade.py" %s"' % (command_parameter_string,))
    return 0


def process_variable(user_command):
    split_var = user_command.split(' ', 2)
    if len(split_var) == 1:
        for key in config['VARIABLES']:
            print('%s: %s' % (key, config['VARIABLES'][key],))
        return 0
    var_name = split_var[1].lower()
    if var_name in PREDEFINED_VARIABLES:
        print('Variable name [%s] is not allowed.' % (var_name,))
        return 2
    if len(split_var) == 2:
        if var_name not in config['VARIABLES']:
            print('No variable with name [%s] defined.' % (var_name,))
            return 2
        print('%s' % (config['VARIABLES'][var_name],))
        return 0
    var_value = split_var[2]
    config['VARIABLES'][var_name] = var_value
    with open('itrade.ini', 'w') as configfile:
        config.write(configfile)
    return 0


def delete_variable(user_command):
    split_var = user_command.split(' ')
    if len(split_var) < 2:
        print('Variable name expected.')
        return 2
    config.remove_option('VARIABLES', split_var[1])
    with open('itrade.ini', 'w') as configfile:
        config.write(configfile)
    return 0


def delete_alias(user_command):
    split_alias = user_command.split(' ')
    if len(split_alias) < 2:
        print('Alias name expected.')
        return 2
    config.remove_option('ALIAS', split_alias[1])
    with open('itrade.ini', 'w') as configfile:
        config.write(configfile)
    return 0


def process_alias(user_command):
    split_alias = user_command.split(' ', 2)
    if len(split_alias) == 1:
        for key in config['ALIAS']:
            print('%s: %s' % (key, config['ALIAS'][key],))
        return 0
    alias_name = split_alias[1].lower()
    if alias_name in TRADE_COMMANDS:
        print('Alias name [%s] is not allowed.' % (alias_name,))
        return 2
    if len(split_alias) == 2:
        if alias_name not in config['ALIAS']:
            print('No alias with name [%s] defined.' % (alias_name,))
            return 2
        print('%s' % (config['ALIAS'][alias_name],))
        return 0
    alias_command = split_alias[2]
    config['ALIAS'][alias_name] = alias_command
    with open('itrade.ini', 'w') as configfile:
        config.write(configfile)
    return 0


def call_alias_command_list(alias_commands, user_parameter):
    i = 0
    found = True
    while found:
        found = False
        for j in range(len(alias_commands)):
            if '{' + str(i) + '}' in alias_commands[j]:
                if len(user_parameter) > i:
                    alias_commands[j] = alias_commands[j].replace('{' + str(i) + '}', user_parameter[i])
                    found = True
                else:
                    print('Parameter expected!')
                    return 2
        i += 1
    user_parameter = user_parameter[i - 1:]
    alias_commands.extend(user_parameter)
    if len(alias_commands) > 0 and alias_commands[0] in config['ALIAS']:
        next_alias_commands = shlex.split(config['ALIAS'][alias_commands[0]])
        if len(alias_commands) > 1:
            next_user_parameter = alias_commands[1:]
        else:
            next_user_parameter = []
        return call_alias_command_list(next_alias_commands, next_user_parameter)
    return call_trade(alias_commands)


def call_alias(user_command, first_user_command):
    alias_commands = shlex.split(config['ALIAS'][first_user_command])
    command_split = user_command.split(' ', 1)
    if len(command_split) > 1:
        user_parameter = shlex.split(command_split[1])
    else:
        user_parameter = []
    return call_alias_command_list(alias_commands, user_parameter)


def python_call(user_command):
    if user_command.strip() == '::':
        os.system('python -i')
        return 2
    command = user_command[1:].strip()
    # noinspection PyBroadException
    try:
        res = eval(command)
        if res is not None:
            print(res)
    except:
        print('Invalid Python expression.')
    return 2


def process_user_command(user_command):
    user_command = user_command.strip()
    if len(user_command) == 0:
        return 2
    first_user_command = user_command.split(' ', 1)[0].lower()
    if first_user_command == ':q' or first_user_command == ':quit':
        return 1
    if first_user_command == ':alias':
        return process_alias(user_command)
    if first_user_command == ':delalias':
        return delete_alias(user_command)
    if first_user_command == ':var':
        return process_variable(user_command)
    if first_user_command == ':delvar':
        return delete_variable(user_command)
    if first_user_command in config['ALIAS']:
        return call_alias(user_command, first_user_command)
    if user_command[0] == ':':
        return python_call(user_command)

    return call_trade(shlex.split(user_command))


def main(argv):
    if len(argv) > 0 and argv[0] == '--version':
        print('itrade version ' + VERSION)
        exit(0)
    config.read(os.path.dirname(os.path.realpath(__file__)) + '/itrade.ini')
    if 'ALIAS' not in config:
        config['ALIAS'] = {}
    if 'VARIABLES' not in config:
        config['VARIABLES'] = {}
    if 'MAIN' not in config:
        print('Missing [MAIN] section in itrade.ini')
        exit(1)
    if 'tradepath' not in config['MAIN']:
        print('Missing [MAIN]->tradepath property in itrade.ini')
        exit(1)

    while True:
        user_command = input('>>: ')
        process_code = process_user_command(user_command)
        if process_code == 2:
            continue
        elif process_code != 0:
            break

if __name__ == "__main__":
    main(sys.argv[1:])
